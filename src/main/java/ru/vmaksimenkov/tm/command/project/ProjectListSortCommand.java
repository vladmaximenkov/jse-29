package ru.vmaksimenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.enumerated.Sort;
import ru.vmaksimenkov.tm.exception.entity.SortNotFoundException;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public final class ProjectListSortCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show project list sorted";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-list-sort";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[PROJECT LIST SORTED]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable List<Project> list;
        try {
            list = serviceLocator.getProjectService().findAll(userId, Sort.getSort(TerminalUtil.nextLine()).getComparator());
        } catch (@NotNull final SortNotFoundException e) {
            System.err.println(e.getMessage() + " Default sort instead");
            list = serviceLocator.getProjectService().findAll(userId);
        }
        System.out.printf("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s %n", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED");
        @NotNull AtomicInteger index = new AtomicInteger(1);
        if (list == null) return;
        list.forEach((x) -> System.out.println(index.getAndIncrement() + "\t" + x));
    }

}
